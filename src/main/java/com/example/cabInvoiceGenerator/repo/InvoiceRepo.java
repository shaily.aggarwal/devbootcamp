package com.example.cabInvoiceGenerator.repo;

import com.example.cabInvoiceGenerator.Ride;

import java.util.List;

public interface InvoiceRepo {

    public List<Ride> getRides(String userId);
}
