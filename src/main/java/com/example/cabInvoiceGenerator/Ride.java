package com.example.cabInvoiceGenerator;

public class Ride {

    private static final int PRICE_PER_KM = 10;
    private static final int PRICE_PER_MINUTE = 1;
    private static final int MINIMUM_FARE = 5;
    
    private double distance;
    private int duration;

    public Ride(double distance, int duration) {
        this.distance=distance;
        this.duration=duration;
    }

    public double getDistance() {
        return distance;
    }

    public int getDuration() {
        return duration;
    }

    public double fare() {
        double fare = PRICE_PER_KM * getDistance() + PRICE_PER_MINUTE * getDuration();
        if (fare < MINIMUM_FARE) return MINIMUM_FARE;
        return fare;
    }
}
