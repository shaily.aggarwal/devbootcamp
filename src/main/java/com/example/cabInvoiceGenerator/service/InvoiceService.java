package com.example.cabInvoiceGenerator.service;

import com.example.cabInvoiceGenerator.Invoice;
import com.example.cabInvoiceGenerator.repo.InvoiceRepo;

public class InvoiceService {
    InvoiceRepo invoiceRepo;

    public InvoiceService(InvoiceRepo invoiceRepo) {
        this.invoiceRepo = invoiceRepo;
    }

    public Invoice getInvoice(String userId) {
        return new Invoice(invoiceRepo.getRides(userId));
    }
}
