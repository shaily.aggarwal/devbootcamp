package com.example.cabInvoiceGenerator;

import java.util.List;

public class Invoice {
    private List<Ride> rides;

    public Invoice(List<Ride> rides) {
        this.rides = rides;
    }

    public double totalRides() {
        return rides.size();
    }

    public double averageFare() {
        return totalFare()/totalRides();
    }

    public double totalFare() {
        double totalFare= 0f;
        for (Ride ride : rides)
            totalFare += ride.fare();

        return totalFare;
    }
}
