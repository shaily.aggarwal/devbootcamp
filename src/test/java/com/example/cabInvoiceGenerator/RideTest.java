package com.example.cabInvoiceGenerator;

import org.junit.Test;

import static org.junit.Assert.*;

public class RideTest {
    @Test
    public void shouldReturnMinimumFare() {
        Ride ride = new Ride(0,1);
        double fare = ride.fare();
        assertEquals(5, fare, 0.01);
    }

    @Test
    public void shouldReturnFareForValidDistanceAndDuration() {
        Ride ride = new Ride(2,1);
        double fare = ride.fare();
        assertEquals(21, fare, 0.01);
    }
}
