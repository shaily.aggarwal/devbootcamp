package com.example.cabInvoiceGenerator;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InvoiceTest {

    private Invoice invoice;

    @Before
    public void initialize(){
        Ride ride = new Ride(2, 1);
        Ride another = new Ride(2, 1);

        List<Ride> rides = new ArrayList<Ride>();
        rides.add(ride);
        rides.add(ride);

        invoice =  new Invoice(rides);
    }

    @Test
    public void shouldReturnFareForMultipleRide() {

        double totalFare = invoice.totalFare();
        assertEquals(42, totalFare, 0);
    }

    @Test
    public void shouldReturnTotalRides() {
        assertEquals(2, invoice.totalRides(), 0);
    }
    @Test
    public void shouldReturnAverageFare() {
        assertEquals(21, invoice.averageFare(), 0);
    }
}
