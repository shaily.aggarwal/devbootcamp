package com.example.cabInvoiceGenerator.service;

import com.example.cabInvoiceGenerator.Invoice;
import com.example.cabInvoiceGenerator.Ride;
import com.example.cabInvoiceGenerator.repo.InvoiceRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class InvoiceServiceTest {
    String userId="1";

    @Mock
    InvoiceRepo invoiceRepo;

    List<Ride> rides = new ArrayList<Ride>();

    @Before
    public void setUp(){
        Ride ride = new Ride(2, 1);
        Ride another = new Ride(2, 1);

        rides.add(ride);
        rides.add(another);
    }

    @Test
    public void shouldReturnInvoiceForUser() {
        initMocks(this);
        when(invoiceRepo.getRides(userId)).thenReturn(rides);
        InvoiceService invoiceService=new InvoiceService(invoiceRepo);
        Invoice invoice= invoiceService.getInvoice(userId);
        assertEquals(42,invoice.totalFare(),0);
    }
}
